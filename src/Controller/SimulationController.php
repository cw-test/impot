<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SimulationController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('simulation/index.html.twig');
    }

    /**
     * @Route("/simulation/1", name="simulation_show")
     */
    public function show(): Response
    {
        return $this->render('simulation/show.html.twig');
    }
}
