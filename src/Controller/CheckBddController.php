<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckBddController extends AbstractController
{
    /**
     * @Route("/check/bdd", name="check_bdd")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->connect();
        $connected = $em->getConnection()->isConnected();
        dump($connected);

        return $this->render('check_bdd/index.html.twig', [
            'controller_name' => 'CheckBddController',
        ]);
    }
}
