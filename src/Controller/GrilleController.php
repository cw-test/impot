<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GrilleController extends AbstractController
{
    /**
     * @Route("/grille", name="grille")
     */
    public function index(): Response
    {
        return $this->render('grille/index.html.twig', [
            'controller_name' => 'GrilleController',
        ]);
    }
}
